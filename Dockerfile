FROM golang:1.15-alpine AS dependancies

# Docker arguments

# Environment Variables
ENV GOPROXY "https://proxy.golang.org,direct"
ENV CGO_ENABLED 0
ENV GOOS linux 
ENV GOARCH amd64

WORKDIR /app
# Add git and curl support
RUN apk add --update git curl

# pull dependancies for cahcing
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy application into image
COPY . .

# Run application with air in dev mode
FROM dependancies AS debug
# Add air support
RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s -- -b /usr/bin
ENTRYPOINT [ "air" ]

HEALTHCHECK --interval=1m --timeout=10s --start-period=20s \
  CMD curl -f http://localhost:8080/_/health || exit 1

# Build application for deployment
FROM dependancies AS build
CMD go build -ldflags '-extldflags "-static"' -v -o /tech-entersekt-crawler .

# Create minimal image with just the application
FROM gcr.io/distroless/static
COPY --from=build /tech-entersekt-crawler /
ENTRYPOINT ["/tech-entersekt-crawler"]
HEALTHCHECK --interval=1m --timeout=10s --start-period=20s \
  CMD curl -f http://localhost:8080/_/health || exit 1