## How to run
- git clone https://gitlab.com/wgmouton/tech-entersekt-crawler.git or git clone git@gitlab.com:wgmouton/tech-entersekt-crawler.git
- cd tech-entersekt-crawler
- docker-compose up

### How to configure.
- Currently the environment variable INDEX_PATH is set to "/" (root) in the docker-compose file. You can update that if you wish to only index a specific directory.

- In the folder ".postman" you will find a postman collection that would be ready for you to import


## How this works

The direction I decided to take was to intentially limit the amount of file io requests to the storage layer. I achieved this by indexing all the files on the INDEX_PATH environment variable. The initial startup time takes a bit longer but repeditive querys will be as fast as the jons can be rendered and transmitted over http. 

I admit this might be an overcomplited way to solve a simple problem, but I wanted to demo something with concurency and allow it to handle volumes of requests in a manner that might not initally overload the server.

This is written in a simliar way as I would have tacled the issue using an actor style archtecture in Elixir using its Genservers to encapsulate some state inside the process itself.

## Routes
- POST: localhost:8080/index
  This reindexes the stored files and would be the set why to get updates if the file system had any changes.

Each one of the endpoints below take a query parameter called path. This is used to indicate which path you which to see. If its ommited then it will return all files that are currently indexed.

- GET: localhost:8080/list/all?path=/app
  This would get all nested files along that path.  

- GET: localhost:8080/list/dicrectory?path=/app
  This would get all files for that specific directory.

- GET: localhost:8080/tree/object?path=/app
  Formats all the nested files along that path in a tree like object. This is expense and might yeld worse results compared to the list endpoints

- GET: localhost:8080/tree/list?path=/app
  Formats all the nested files along that path in a tree like list object. This is expense and might yeld worse results compared to the list endpoints


## There are some shortcommings.

- I would have liked to and fs watch lib included to monitior for changes for a file has been indexed, however I did not want to inlude another external library for this project and felt that a custom implementation was out of scope.

- I would have liked to provide more comments, documention and Unit tests inside the project, but time got the beter of me.

