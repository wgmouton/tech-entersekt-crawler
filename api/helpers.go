package api

import "github.com/prometheus/common/log"

func reoverFromPanic() {
	if r := recover(); r != nil {
		log.Warn("Recovered in f", r)
	}
}

