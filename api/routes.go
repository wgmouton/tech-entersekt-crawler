package api

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/common/log"
	"gitlab.com/wgmouton/tech-entersekt-crawler/crawler"
	"gitlab.com/wgmouton/tech-entersekt-crawler/daemon/walker"
)

func routes(router *gin.Engine) *gin.Engine {
	router.GET("_/health", func(c *gin.Context) {
		log.Debug("Checking Health...")
		c.Status(200)
	})

	router.GET("/list/directory", func(c *gin.Context) {
		defer reoverFromPanic()
		path := c.Query("path")
		files, err := crawler.ListDirctory(c, path)
		if err != nil {
			c.String(504, err.Error())
			return
		}
		c.JSON(200, files)
	})

	router.GET("/list/all", func(c *gin.Context) {
		defer reoverFromPanic()
		path := c.Query("path")
		files, err := crawler.ListAll(c, path)
		if err != nil {
			c.String(504, err.Error())
			return
		}
		c.JSON(200, files)
	})

	router.GET("/tree/object", func(c *gin.Context) {
		defer reoverFromPanic()
		path := c.Query("path")
		dir, err := crawler.TreeObject(c, path)
		if err != nil {
			c.String(504, err.Error())
			return
		}

		c.JSON(200, dir)
	})

	router.GET("/tree/list", func(c *gin.Context) {
		defer reoverFromPanic()
		path := c.Query("path")
		dir, err := crawler.TreeList(c, path)
		if err != nil {
			c.String(504, err.Error())
			return
		}

		c.JSON(200, dir)
	})

	router.POST("/index", func(c *gin.Context) {
		defer reoverFromPanic()
		go walker.Reindex()
		c.Status(202)
	})
	return router
}
