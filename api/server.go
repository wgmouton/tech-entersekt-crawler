package api

import (
	"github.com/gin-gonic/contrib/gzip"
	"github.com/gin-gonic/gin"
)

// Start - Starts API server
func Start() {
	router := gin.Default()
	router.Use(gzip.Gzip(gzip.DefaultCompression))
	routes(router)
	router.Run()
}
