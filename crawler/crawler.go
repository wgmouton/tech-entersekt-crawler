package crawler

import (
	"context"
	"errors"
	"time"

	"github.com/prometheus/common/log"
	"gitlab.com/wgmouton/tech-entersekt-crawler/daemon/walker"
)

func ListDirctory(c context.Context, path string) (responseInfo ResponseInfo, err error) {
	ctx, cancelFunc := context.WithTimeout(c, 10*time.Second)
	resultChan := make(chan ResponseInfo, 1)

	go func() {
		files, _ := walker.GetList(path)
		resultChan <- ResponseInfo{
			TotalFileCount: len(files),
			Requestedpath:  path,
			Contents:       files,
		}
	}()

	select {
	case <-ctx.Done():
		log.Error("Failed to build directory tree within 10 seconds")
		err = errors.New("failed to build directory tree within 10 seconds")
	case responseInfo = <-resultChan:
	}
	close(resultChan)
	cancelFunc()
	return
}

func ListAll(c context.Context, path string) (responseInfo ResponseInfo, err error) {
	ctx, cancelFunc := context.WithTimeout(c, 10*time.Second)
	resultChan := make(chan ResponseInfo, 1)

	go func() {
		files, _ := walker.GetAll(path)
		resultChan <- ResponseInfo{
			TotalFileCount: len(files),
			Requestedpath:  path,
			Contents:       files,
		}
	}()

	select {
	case <-ctx.Done():
		log.Error("Failed to build directory tree within 10 seconds")
		err = errors.New("failed to build directory tree within 10 seconds")
	case responseInfo = <-resultChan:
	}
	close(resultChan)
	cancelFunc()
	return
}

func TreeObject(c context.Context, path string) (responseInfo ResponseInfo, err error) {
	ctx, cancelFunc := context.WithTimeout(c, 10*time.Second)
	resultChan := make(chan ResponseInfo, 1)

	go func() {
		files, _ := walker.GetAll(path)
		var dir DirObject = DirObject{
			Files:       map[string]walker.File{},
			Directories: map[string]DirObject{},
		}

		walkObject(files, path, &dir)

		resultChan <- ResponseInfo{
			TotalFileCount: len(files),
			Requestedpath:  path,
			Contents:       dir,
		}

	}()

	select {
	case <-ctx.Done():
		log.Error("Failed to build directory tree within 10 seconds")
		err = errors.New("failed to build directory tree within 10 seconds")
	case responseInfo = <-resultChan:
	}
	close(resultChan)
	cancelFunc()
	return
}

func TreeList(c context.Context, path string) (responseInfo ResponseInfo, err error) {
	ctx, cancelFunc := context.WithTimeout(c, 10*time.Second)
	resultChan := make(chan ResponseInfo, 1)

	go func() {
		files, _ := walker.GetAll(path)

		var dir DirList = DirList{
			Path:        path,
			Files:       []walker.File{},
			Directories: []DirList{},
		}

		walkList(files, &dir)

		resultChan <- ResponseInfo{
			TotalFileCount: len(files),
			Requestedpath:  path,
			Contents:       dir,
		}

	}()

	select {
	case <-ctx.Done():
		log.Error("Failed to build directory tree within 10 seconds")
		err = errors.New("failed to build directory tree within 10 seconds")
	case responseInfo = <-resultChan:
	}
	close(resultChan)
	cancelFunc()
	return
}
