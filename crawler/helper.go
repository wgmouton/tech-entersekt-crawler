package crawler

import (
	"path/filepath"
	"strings"

	"gitlab.com/wgmouton/tech-entersekt-crawler/daemon/walker"
)

func walkObject(files []walker.File, dirPath string, dir *DirObject) {
	dirFiles := []walker.File{}

	for _, file := range files {
		if filepath.HasPrefix(file.Directory, dirPath) {
			dirFiles = append(dirFiles, file)
		}
	}

	for _, file := range dirFiles {
		if file.IsDirectory && strings.EqualFold(dirPath, file.Directory) {
			subDir := DirObject{
				Size:        file.StringSize,
				Files:       map[string]walker.File{},
				Directories: map[string]DirObject{},
			}
			walkObject(files, file.AbsolutePath, &subDir)
			dir.Directories[file.Name] = subDir
		} else if strings.EqualFold(dirPath, file.Directory) {
			dir.Files[file.Name] = file
		}
	}
}

func walkList(files []walker.File, dir *DirList) {
	dirFiles := []walker.File{}

	for _, file := range files {
		if filepath.HasPrefix(file.Directory, dir.Path) {
			dirFiles = append(dirFiles, file)
		}
	}

	for _, file := range dirFiles {
		if file.IsDirectory && strings.EqualFold(dir.Path, file.Directory) {
			subDir := DirList{
				Size:        file.StringSize,
				Path:        file.AbsolutePath,
				Files:       []walker.File{},
				Directories: []DirList{},
			}
			walkList(dirFiles, &subDir)
			dir.Directories = append(dir.Directories, subDir)
		} else if strings.EqualFold(dir.Path, file.Directory) {
			dir.Files = append(dir.Files, file)
		}
	}

}
