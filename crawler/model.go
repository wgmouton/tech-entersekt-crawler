package crawler

import "gitlab.com/wgmouton/tech-entersekt-crawler/daemon/walker"

type DirList struct {
	Size        string        `json:"size"`
	Path        string        `json:"path"`
	Files       []walker.File `json:"files"`
	Directories []DirList     `json:"directories"`
}

type DirObject struct {
	Size        string                 `json:"size"`
	Files       map[string]walker.File `json:"files"`
	Directories map[string]DirObject   `json:"directories"`
}

type ResponseInfo struct {
	TotalFileCount int         `json:"total_file_count"`
	Requestedpath  string      `json:"requested_path"`
	Contents       interface{} `json:"contents"`
}
