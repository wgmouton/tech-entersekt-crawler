package walker

import (
	"fmt"
	"math"
)

func bytesToMB(bytes int64) string {
	sizeKB := float64(bytes) / 1024.0
	if sizeKB >= 1024 {
		sizeMB := float64(sizeKB) / 1024.0
		sizeMB = math.Round(sizeMB)
		return fmt.Sprintf("%f MB", sizeMB)
	} else {
		sizeKB = math.Round(sizeKB)
		return fmt.Sprintf("%f KB", sizeKB)
	}
}

func publish(messageF message) {
	go func() {
		for {
			select {
			case eventQueue <- messageF:
				return
			}
		}
	}()
}

func publishAsync(messageF messageAsync) {
	go func() {
		for {
			select {
			case eventQueue <- func(state *state) *state {
				go messageF(state)
				return state
			}:
				return
			}
		}
	}()
}
