package walker

import (
	"sync"
	"time"
)

type watcherState struct {
	indexing bool
}

type message func(state *state) *state
type messageAsync func(state *state)

type state struct {
	mutex sync.RWMutex
	value map[string]File
}

type File struct {
	Name         string    `json:"name"`
	AbsolutePath string    `json:"absolute_path"`
	Size         int64     `json:"-"`
	StringSize   string    `json:"size"`
	Directory    string    `json:"-"`
	IsDirectory  bool      `json:"is_directory"`
	LastUpdated  time.Time `json:"updated_at"`
}
