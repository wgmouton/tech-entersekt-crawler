package walker

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/prometheus/common/log"
)

// Daemon Event queue
var eventQueue chan message

// Private funcs
func updateDirectorySize(directoryPath string, size int64) {
	publish(func(fileCache *state) *state {
		fileCache.mutex.Lock()
		defer fileCache.mutex.Unlock()

		if directory, ok := fileCache.value[directoryPath]; ok {
			size := directory.Size + size
			directory.Size = size
			directory.StringSize = bytesToMB(size)
			fileCache.value[directoryPath] = directory
			if !strings.EqualFold(directory.Directory, "/") {
				updateDirectorySize(directory.Directory, directory.Size)
			}
		}

		return fileCache
	})
}

func addFile(path string, info os.FileInfo) {
	publish(func(state *state) *state {
		file := File{
			Name:         info.Name(),
			AbsolutePath: path,
			Size:         info.Size(),
			StringSize:   bytesToMB(info.Size()),
			Directory:    filepath.Dir(path),
			IsDirectory:  info.IsDir(),
			LastUpdated:  info.ModTime(),
		}

		if !file.IsDirectory {
			updateDirectorySize(file.Directory, file.Size)
		}

		state.mutex.Lock()
		state.value[path] = file
		state.mutex.Unlock()
		return state
	})
}

func flush() {
	publish(func(_ *state) *state {
		log.Warn("Flushing cache")
		return &state{value: make(map[string]File)}
	})
}

func index() {
	publishAsync(func(_ *state) {
		indexPath, empty := os.LookupEnv("INDEX_PATH")
		if !empty || len(indexPath) == 0 {
			indexPath = "/"
			log.Warnf("Index path not was not specified, defaulting to \"%s\"", indexPath)
		}

		// state.indexing = true
		log.Infof("Indexing... \"%s\"", indexPath)
		indexedFiles := 0

		err := filepath.Walk(indexPath, func(path string, info os.FileInfo, err error) error {
			if info != nil {
				indexedFiles++
				addFile(path, info)
			} else {
				log.Debug("File did not contain any metadata:", path)
			}
			return nil
		})

		if err != nil {
			log.Error(err)
		}

		log.Infof("Indexed %d files", indexedFiles)
		// state.indexing = false
	})
}

// Public Funcs

func Start() {
	eventQueue = make(chan message, 1000)
	fileCache := state{
		value: map[string]File{},
	}

	go func() {
		for {
			<-time.After(5 * time.Second)
			log.Infof("queue size: %d", len(eventQueue))
		}
	}()

	go func() {
		log.Info("Starting Walker Daemon")
		for {
			// var temp map[string]File = make(map[string]File)
			// temp = fileCache
			select {
			case message, ok := <-eventQueue:
				if !ok {
					break
				}
				message(&fileCache)
			}
		}
	}()

	// Index dir
	index()
}

func Reindex() {
	// if state.indexing {
	// log.Info("Already indexing. will wait for next event.")
	// } else {
	flush()
	index()
	// }
}

func GetList(path string) (result []File, err error) {
	resultChan := make(chan []File, 1)
	errChan := make(chan error, 1)

	publishAsync(func(fileCache *state) {
		var files []File = []File{}

		fileCache.mutex.Lock()
		for _, file := range fileCache.value {
			if strings.EqualFold(file.Directory, path) {
				files = append(files, file)
			}
		}
		fileCache.mutex.Unlock()

		resultChan <- files
	})

	select {
	case result = <-resultChan:
		// log.Debug("Returned", len(result), "files")
	case err = <-errChan:
	}
	close(resultChan)
	close(errChan)
	return
}

func GetAll(path string) (result []File, err error) {
	resultChan := make(chan []File, 1)
	errChan := make(chan error, 1)

	publishAsync(func(fileCache *state) {
		var files []File = []File{}

		fileCache.mutex.Lock()
		for filePath, file := range fileCache.value {
			if strings.HasPrefix(filePath, path) {
				files = append(files, file)
			}
		}
		fileCache.mutex.Unlock()

		resultChan <- files
	})

	select {
	case result = <-resultChan:
		// log.Debug("Returned", len(result), "files")
	case err = <-errChan:
	}
	close(resultChan)
	close(errChan)
	return
}
