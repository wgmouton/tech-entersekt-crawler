package main

import (
	"sync"

	"gitlab.com/wgmouton/tech-entersekt-crawler/api"
	"gitlab.com/wgmouton/tech-entersekt-crawler/daemon/walker"
)

func main() {
	var wg sync.WaitGroup

	wg.Add(2)
	go walker.Start()
	go api.Start()
	wg.Wait()
}
